.. _config-core:

========================
Configuring Mailman Core
========================

If you are here, it means that you have successfully installed complete Mailman
3 or some of its components. To verify that you have Mailman Core installed
try::

    $ mailman info
    GNU Mailman 3.1.0 (Between The Wheels)
    Python 3.5.3 (default, Jan 19 2017, 14:11:04)
    [GCC 6.3.0 20170118]
    config file: /etc/mailman.cfg
    db url: sqlite:////var/lib/mailman/data/mailman.db
    devmode: DISABLED
    REST root url: http://localhost:8001/3.1/
    REST credentials: restadmin:restpass

There are a few important parameters in the above command that can help you
debug problems if they occur. Most important of which is the configuration file
that is being used, pointed to by ``config_file`` that you see above.

Mailman 3 uses a total of two main configuration files, the first one is for
Mailman Core and the other is Django's ``settings.py`` used to deploy the web
front end.

``mailman.cfg`` is the main configuration file for Mailman Core. Core will look
for this file at several different places in order like:

    * Filesystem Path pointed to by ``MAILMAN_CONFIG_FILE`` environment
      variable.

    * ``mailman.cfg`` in the current working directory.

    * ``var/etc/mailman.cfg`` relative to the current working directory.

    * ``$HOME/.mailman.cfg``

    * ``/etc/mailman.cfg``

    * ``../../etc/mailman.cfg`` relative to the working directory of ``argv[0]``
      i.e. the working directory of the ``mailman`` command script.

Alternatively, you can specify the absolute path of the configuration file using
the ``-C`` flag in the command line.

.. _config-core-fhs:

Configuring Filesystem Paths
============================

Mailman by default puts all the configuration, logs and lock files in the
current working directory under a ``var`` directory. Each ``var`` directory
belongs to either a running or previously run Mailman Core instance.

While it is often confusing to several people, the reason it is default is
because it helps a lot with debugging and testing. In production you want a
static location for your configuration, locks and logs.

All the different paths that Mailman creates are, by default, configured
relative to ``var_dir``. For example, ``paths.fhs`` which is based on
`Filesystem Hierarchy Standard <http://www.pathname.com/fhs/pub/fhs-2.3.html>`_
. To use it add the following to your configuration::

  [mailman]
  layout: fhs

This  is equivalent to the following configuration::

  bin_dir: /sbin
  var_dir: /var/lib/mailman
  queue_dir: /var/spool/mailman
  log_dir: /var/log/mailman
  lock_dir: /var/lock/mailman
  etc_dir: /etc
  ext_dir: /etc/mailman.d
  pid_file: /var/run/mailman/master.pid

Most of the path names are self explanatory, like all the runnable scripts are
put under */sbin* and all the logs are placed under */var/log/mailman*.

If you want a custom directory layout you can define a new layout and use that
instead. For example::

  [paths.custom]
  var_dir: /home/user/.mailman

  [mailman]
  layout: custom

If you are installing Mailman from source (or using Pip), it is recommended to
use ``paths.local``. To use it, add the following to your ``mailman.cfg``::

  [mailman]
  layout: local

It is equivalent to the following configuration::

  var_dir: /var/tmp/mailman
  bin_dir: $argv
  log_dir: $var_dir/logs
  lock_dir: $var_dir/locks
  data_dir: $var_dir/data
  cache_dir: $var_dir/cache
  etc_dir: $var_dir/etc
  messages_dir: $var_dir/messages
  archive_dir: $var_dir/archives
  template_dir: $var_dir/templates
  pid_file: $var_dir/master.pid
  lock_file: $lock_dir/master.lck

.. _config-core-mta:

Configuring MTA
===============

The first step to get Core to work is to enable it to talk to the Mail Transport
Agent or MTA. It supports various different MTAs.

.. _config-core-postfix:

`Postfix`_
    Core automatically generated transport maps(``postfix_lmtp`` and
    ``postfix_lmtp``) to be used by Postfix at ``var/data/``. To configure
    Postfix, add the following configuration to ``main.cf``::

      # Support the default VERP delimiter.
      recipient_delimiter = +
      unknown_local_recipient_reject_code = 550
      owner_request_special = no
      transport_maps =
          hash:/path-to-mailman/var/data/postfix_lmtp
      local_recipient_maps =
          hash:/path-to-mailman/var/data/postfix_lmtp
      relay_domains =
          hash:/path-to-mailman/var/data/postfix_domains

    Mailman's ``var`` directory can vary according your source of
    installation. Please refer to the documentation provided with your source or
    ask Mailman Developers at mailman-developers@python.org.

    To configure Core to use Postfix, add the following configuration to your
    ``mailman.cfg`` configuration, be sure to replace ``mail.example.com`` with
    your email domain::

      [mta]
      incoming: mailman.mta.postfix.LMTP
      outgoing: mailman.mta.deliver.deliver
      lmtp_host: mail.example.com
      lmtp_port: 8024
      smtp_host: mail.example.com
      smtp_port: 25

.. _config-core-exim:

`Exim4`_
    To setup Exim4, add the following files to your configuration as
    file names mentioned above::

      # /etc/exim4/conf.d/main/25_mm3_macros
      # The colon-separated list of domains served by Mailman.
      domainlist mm_domains=list.example.net

      MM3_LMTP_PORT=8024

      # MM3_HOME must be set to Mailman's var directory, wherever it is
      # according to your installation.
      MM3_HOME=/path-to-mailman/var/
      MM3_UID=list
      MM3_GID=list

      ################################################################
      # The configuration below is boilerplate:
      # you should not need to change it.

      # The path to the list receipt (used as the required file when
      # matching list addresses)
      MM3_LISTCHK=MM3_HOME/lists/${local_part}.${domain}

      # /etc/exim4/conf.d/router/455_mm3_router
      mailman3_router:
      driver = accept
      domains = +mm_domains
      require_files = MM3_LISTCHK
      local_part_suffix_optional
      local_part_suffix = \
      -bounces   : -bounces+* : \
      -confirm   : -confirm+* : \
      -join      : -leave     : \
      -owner     : -request   : \
      -subscribe : -unsubscribe
      transport = mailman3_transport

      # /etc/exim4/conf.d/transport/55_mm3_transport
      mailman3_transport:
      driver = smtp
      protocol = lmtp
      allow_localhost
      hosts = localhost
      port = MM3_LMTP_PORT
      rcpt_include_affixes = true

    You should note that Exim4 configuration above doesn't support multiple
    domains like Postfix does. Please change the variables in the configuration
    above before installing it.

    To configure Mailman to use Exim4 add the following to your
    ``mailman.cfg`` ::

      [mta]
      incoming: mailman.mta.exim4.LMTP
      outgoing: mailman.mta.deliver.deliver
      lmtp_host: mail.example.com
      smtp_host: mail.example.com
      lmtp_port: 8024
      smtp_port: 25

Other MTAs
    Mailman also supports `Sendmail`_ and `qmail`_. Please check the Mailman
    Core documentation for `sendmail setup`_ and `qmail setup`_ to configure
    them.

.. _config-core-api:

Configuring REST API
====================

Core presents a HTTP Rest API which clients can use to interact with it. Note
that this is an administrative API and **must** not be exposed to the public
internet. It has a very basic HTTP Basic Authentication which can be configured
using the configuration below::

  [webservice]
  hostname: localhost
  port: 8001
  use_https: no
  admin_user: restadmin
  admin_pass: restpass
  api_version: 3.1

If you need to bind to a different host or port, you can change the
configuration above according to you needs.

There are several different other ways to configure Core and you can find more
details about it in the `Core's documentation`_.


.. _config-core-templates:

Configure Templates
===================

You can configure templates for headers, footers and automated emails generated
by Mailman. By default, the only templates available are in English, but you can
add them other languages too. You can add templates to the var directory of
Mailman, which will be picked up by Core.


* sitewide templates can be put in $var/templates/site/LC/,
* domain specific templates in var/templates/domains/DOMAIN/LC/
* list specific templates invar/templates/lists/LIST-ID/LC/


List overrides domain overrides site overrides defaults.


.. _config-core-database:

Configuring Databases
=====================

Core supports Sqlite, PostgreSQL and MySQL and each of them are tested. By
default, without any configuration, Core uses Sqlite database. For a production
use, a more efficient database like PostgreSQL or MySQL is highly
recommended. Please have a look at the `setting up your database`_ in Core's
documentation for the same.

.. _config-core-cron-jobs:

Configuring Cron Jobs
=====================

Depending on your configuration, Core has some periodic tasks that need to be
run using job schedulers like ``cron``. Right now, the two tasks required to be
run routinely for Core are periodic sending of digests for mailing lists that
have ``digest_send_periodic`` set to ``true`` and periodic sending of notices
of pending requests to list moderators. These can be customized to send at
any periodic time, the following format sends out digests at midnight::

  @daily /path/to/mailman digests --periodic

This format will send moderator notices at 08:00::

  0 8 * * * /path/to/mailman notify

If your installation has lists which make use of the `NNTP Gateway`_ to gate
messages from ``Usenet`` to one or more lists, you also need to periodically
run the ``mailman gatenews`` command to gate messages from ``Usenet`` to the
list(s). This format will run the command every 5 minutes::

  */5 * * * * /path/to/mailman gatenews

.. note:: 

  Be careful about mailman.cfg locations.

  If your mailman.cfg is not found in a standard location, mailman will silently
  write a new emtpy mailman.cfg in ``~/var/etc`` and terminate successfully. If your
  notify and digests jobs are failing silently, try adding the -C option to
  specify the config file to use.
.. _`Core's documentation`: http://mailman.readthedocs.io/en/latest/src/mailman/config/docs/config.html
.. _`setting up your database`: http://mailman.readthedocs.io/en/latest/src/mailman/docs/database.html
.. _Postfix: http://www.postfix.org/
.. _Exim4: http://www.exim.org/
.. _sendmail: https://www.proofpoint.com/us/sendmail-open-source
.. _qmail: http://cr.yp.to/qmail.html
.. _sendmail setup: https://mailman.readthedocs.io/en/latest/src/mailman/docs/mta.html#sendmail
.. _qmail setup : https://mailman.readthedocs.io/en/latest/src/mailman/docs/mta.html#qmail-configuration
.. _NNTP Gateway: https://mailman.readthedocs.io/en/latest/src/mailman/handlers/docs/nntp.html
